@echo OFF

if "%1"=="" goto usage

set CMD=%1git.exe
@echo CMD=%CMD%
@echo 
%1git.exe git pull --rebase origin master
%1git.exe git push origin master

goto finish

:usage
@echo ON
@echo - Usage
@echo -  
@echo - This will push up master
@echo -
@echo - To configure with SourceTree 
@echo -     open Tools\Options\Custom Actions 
@echo -     click Add
@echo -     enter a Menu Caption
@echo -     enter the path to the script
@echo -     Add the path to the git.exe (PARAMETER)

:finish

