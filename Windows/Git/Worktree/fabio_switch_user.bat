@echo OFF

if "%1"=="" goto usage

set CMD=%1git.exe

@echo - NEW USER 
@echo - Username : %2
@echo - User Email : %3

%1git.exe config --global user.name "%2"
%1git.exe config --global user.email %3
%1git.exe config --list

goto finish

:usage
@echo ON
@echo - Usage
@echo -  
@echo - This will switch git user 
@echo -
@echo - To configure with SourceTree 
@echo -     open Tools\Options\Custom Actions 
@echo -     click Add
@echo -     enter a Menu Caption
@echo -     enter the path to the script
@echo -     Add the path to the git.exe (FIRST PARAMETER)
@echo -     Add Username of the new user (SECOND PARAMETER)
@echo -     Add User Eamil of the new user (THIRD PARAMETER)
@echo -  
@echo - Example:
@echo -  
@echo - "c:\my-repo>fabio_switch_user"
@echo -  
@echo - This will list the worktrees referencing the repo in the directory c:\my-repo
@echo -  
@echo - "c:\my-repo>LIst-WorkTree 'c:\program files(x86)\git\cmd\'"
@echo -  
@echo - This will list the worktrees referencing the repo in the directory c:\my-repo
@echo - using the git.exe found at c:\program files(x86)\git\cmd\
@echo - a trailing \ is required

:finish
