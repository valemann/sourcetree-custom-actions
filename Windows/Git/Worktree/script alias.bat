 
@echo OFF

if "%1"=="" goto usage

set CMD=%1git.exe


%1git.exe git config --global alias.co checkout  
%1git.exe git config --global alias.ci commit 
%1git.exe git config --global alias.st status  
%1git.exe git config --global alias.br branch  
%1git.exe git config --list
goto finish

:usage
@echo ON
@echo - Usage
@echo -  
@echo - This script  will add some useful alias

:finish