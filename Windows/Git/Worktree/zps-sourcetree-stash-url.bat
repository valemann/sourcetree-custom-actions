@echo off
set GHash=%~1

rem Set the GUrl from cmd result
for /f "delims=" %%i in ('%~dp0/stash_base.bat') do set GUrl=%%i

rem Use clip to copy to the clipboard
echo %GUrl%/commits/%GHash% | clip
