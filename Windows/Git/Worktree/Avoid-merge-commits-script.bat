@echo OFF

if "%1"=="" goto usage

set CMD=%1git.exe

%1git config --global branch.autosetuprebase always

goto finish

:usage
@echo ON
@echo - Usage
@echo -  
@echo - This script overwrite git default setting to create merge commits if you pull in divergent changes.
@echo -
:finish
