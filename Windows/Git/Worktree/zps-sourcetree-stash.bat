@echo off
set GitHash=%~1

rem Set WebGitAddr var from the cmd result
for /f "delims=" %%i in ('%~dp0/stash_base.bat') do set WebGitAddr=%%i

rem Use 'start' to open
start %WebGitAddr%/browse
