@echo OFF

if "%1"=="" goto usage

set CMD=%1git.exe list
@echo CMD=%CMD%

%1git.exe worktree list

goto finish

:usage
@echo ON
@echo - Usage
@echo -  
@echo - This will list all the current worktrees
@echo - see https://git-scm.com/docs/git-worktree
@echo - Optionally pass the path to the git.exe
@echo -  
@echo - To configure with SourceTree 
@echo -     open Tools\Options\Custom Actions 
@echo -     click Add
@echo -     enter a Menu Caption
@echo -     enter the path to the script
@echo -     Optionally, the path to the git.exe, if it is not available on your path.
@echo -  
@echo - Example:
@echo -  
@echo - "c:\my-repo>List-WorkTree"
@echo -  
@echo - This will list the worktrees referencing the repo in the directory c:\my-repo
@echo -  
@echo - "c:\my-repo>LIst-WorkTree 'c:\program files(x86)\git\cmd\'"
@echo -  
@echo - This will list the worktrees referencing the repo in the directory c:\my-repo
@echo - using the git.exe found at c:\program files(x86)\git\cmd\
@echo - a trailing \ is required

:finish