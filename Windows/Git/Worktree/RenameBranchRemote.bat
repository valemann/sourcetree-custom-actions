@echo OFF

if "%1"=="" goto usage

set CMD=%1git.exe
@echo CMD=%CMD%

@echo - Branch Name
@echo - old_name : %2
@echo - new_name : %3

%1git.exe branch -m %2 -name %3
%1git.exe push origin :%2 %3
%1git.exe push origin -u %3

goto finish

:usage
@echo ON
@echo - Usage
@echo -  
@echo - This script Rename Local and Remote Branch
@echo -
@echo - To configure with SourceTree 
@echo -     open Tools\Options\Custom Actions 
@echo -     click Add
@echo -     enter a Menu Caption
@echo -     enter the path to the script
@echo -     Optionally, the path to the git.exe, if it is not available on your path.
@echo -  
@echo -  
@echo - This will list the worktrees referencing the repo in the directory c:\my-repo
@echo -  
@echo - "c:\my-repo>LIst-WorkTree 'c:\program files(x86)\git\cmd\'"
@echo -  
@echo - This will list the worktrees referencing the repo in the directory c:\my-repo
@echo - using the git.exe found at c:\program files(x86)\git\cmd\
@echo - a trailing \ is required

:finish